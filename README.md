
# monotone-terminal

This is a color scheme based on the idea that syntax highlighting is a sham.

It is heavily inspired by [monotone](https://github.com/Lokaltog/vim-monotone).
The main deviation is that I have chosen to make comments their own color.


The main colors used come from ye odle terminal colors:
![phosterm](./screenshots/phos_term.png)


Here are a few screenshots in case you are lazy like me:
![perl](./screenshots/perl_term.png)
![readme](./screenshots/readme_term.png)
![golang](./screenshots/golang_term.png)
